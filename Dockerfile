FROM rust AS builder
ADD . /app
WORKDIR /app
# RUN cargo build --release

RUN RUSTFLAGS='-C target-feature=+crt-static' cargo build --release --target x86_64-unknown-linux-gnu

FROM scratch
COPY --from=builder /app/target/x86_64-unknown-linux-gnu/release/dotpi-api /
CMD [ "/dotpi-api" ]