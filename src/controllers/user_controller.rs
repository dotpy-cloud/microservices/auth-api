use axum::Json;
use crate::entities::user_entity::UserEntity;
use crate::dto::create_user_dto::CreateUserDTO;

pub async fn create_user(Json(user): Json<CreateUserDTO>) -> Json<UserEntity> {
	return Json(UserEntity {
		id: 0,
		username: user.username.clone(),
		email: user.email.clone(),
		password: user.password.clone(),
	});
}