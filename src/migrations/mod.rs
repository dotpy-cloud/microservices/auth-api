use sea_orm_migration::prelude::*;
use sea_orm_migration::MigrationTrait;

mod m_20241509_bootstrap_database_tables;
pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
	fn migrations() -> Vec<Box<dyn MigrationTrait>> {
		vec![
			Box::new(m_20241509_bootstrap_database_tables::Migration),
		]
	}
}
