use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct CreateUserDTO {
	pub username: String,
	pub email: String,
	pub password: String,
}