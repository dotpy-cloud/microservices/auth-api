use futures::executor::block_on;
use migrations::Migrator;
use sea_orm::{Database, DbErr};

mod migrations;

use sea_orm_migration::prelude::*;

// use sea_orm::{ConnectionTrait, Database, DbBackend, DbErr, Statement};

async fn run() -> Result<(), DbErr> {
	let db_conn_string = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");

	let db = Database::connect(&db_conn_string).await?;

	let schema_manager = SchemaManager::new(&db);
	Migrator::refresh(&db).await?;
	assert!(schema_manager.has_table("bakery").await?);

	Ok(())
}

#[tokio::main]
async fn main() {
	let port = std::env::var("PORT").unwrap_or("3000".to_string());
	println!("Starting server on port {}", port);

	if let Err(e) = block_on(run()) {
		panic!("Error connecting to database: {:?}", e);
	}

	println!("Connected to database");
}
