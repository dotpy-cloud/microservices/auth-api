use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct UserEntity {
	pub id: i32,
	pub username: String,
	pub email: String,
	pub password: String,
}